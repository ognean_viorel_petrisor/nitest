$(function(){
    $("#nav_toggle").click(function(){
        $("#nav_list").slideToggle();
    });
});

// send AJAX request when "SUBMIT" button was clicked
$(document).on('click', '#submit_btn', function (event) {

	// prevent form submission

	event.preventDefault();

	var name = $('#name').val();
	var phone = $('#phone').val();
	var email = $('#email').val();

	// name value is required

	if(name == '') {
		// add "error" class
		$('#name').addClass('ui-state-error');
		return false;
	}
	else {
		// remove "error" class
		$('#name').removeClass('ui-state-error');
	}

	// check what Preferred method of contact was selected

	var pref_method = $("input[name='pref_method']:checked").val();

	// if phone radio button was ticked, Phone input must not be empty
	// if email radio button was ticked, Email input must not be empty
	// anything else is not valid

	if(pref_method == 'phone') {
		if(phone == '') {
			// add "error" class
			$('#phone').addClass('ui-state-error');
			return false;
		}
		else {
			// remove "error" class
			$('#phone').removeClass('ui-state-error');
		}
	}
	else if(pref_method == 'email') {
		if(email == '') {
			// add "error" class
			$('#email').addClass('ui-state-error');
		}
		else {
			// remove "error" class
			$('#email').removeClass('ui-state-error');
		}
	}
	else {
		// no radio button was checked
		// or invalid value
		return false;
	}

	$.ajax({
		type: "POST",
		url: "process.php",
		dataType: "json",
		data: {
			name: name,
			phone: phone,
			email: email,
			pref_method: pref_method
		},
		success: function(data) {
			if(data.status == 'success') {
				window.location = 'index.php';
			}
			else {
				alert('There was an error processing your request, please try again !!!');
			}
		},
		error: function (xhr, status) {
			// display status
			switch (status) {
				case 404:
					alert('File not found');
					break;
				case 500:
					alert('Server error');
					break;
				case 0:
					alert('Request aborted');
					break;
				default:
					alert('Unknown error: ' + xhr.responseText);
			}
		}
	});
});

// validate email if anything was filled / changed before sending it to DB
$(document).on('change','#email',function (event) {

	var email = $('#email').val();

	// validate email address only if something was filled into the input field

	if(email.length > 0) {

		// validate email address
		var valid_email = validateEmail(email);

		if(valid_email) {

			// remove "error" class
			$('#email').removeClass('ui-state-error');

			// continue execution of normal script
			return;
		}
		else {
			// add "error" class
			$('#email').addClass('ui-state-error');

			// display validation results
			alert('Please enter a valid email address !!!');
		}
	}
	else {
		// remove "error" class
		$('#email').removeClass('ui-state-error');
	}
});

// validate phone if anything was filled / changed before sending it to DB
$(document).on('change','#phone',function (event) {

	var current_value = $(this).val();

	var validNumber = validateNumber(current_value);

	if(!validNumber) {

		// add "error" class
		$('#phone').addClass('ui-state-error');

		// display validation results
		alert('Only numbers are allowed !!!');
	}
	else {
		// remove "error" class
		$('#phone').removeClass('ui-state-error');

		// continue execution of normal script
		return;
	}
});

// validate email address
// returns true for single emails or many emails seperated by commas
function validateEmail(email) {

	// lets assume that the email pass the validation
	var test = true;

	var emailReg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

	// emails can be separated by commas
	var email_array = email.split(',');

	if(email_array.length > 10) {
		test = false;
		alert('Please do not provide more then 10 email addresses !!!');
	}
	else {
		// check all emails to be valid
		for(var i = 0; i < email_array.length; i++){ 
			if (!emailReg.test(email_array[i])) {
				// if any value does not pass the validation, return false
				test = false;
			}
		}
	}

	return test;
}

// validate a provided value to be numeric
function validateNumber(number) {

	var regex=/^[0-9]+$/;

	// lets assume that the value is numeric
	var valid = true;

	if (!number.match(regex)) {
		// if anything other then numbers was typed return false
        valid = false;
    }

    return valid;
}
