<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

if(isset($_POST['name'])) {
	$name = trim($_POST['name']);
}
else {
	$name = '';
}

if(!empty($name)) {

	if(isset($_POST['pref_method'])) {
		$pref_method = trim($_POST['pref_method']);
	}
	else {
		$pref_method = '';
	}

	if(!empty($pref_method)) {

		require_once 'classes/database.php';
		require_once 'classes/util.php';
		$db   = Database::get();
		$list = $db->selectAll('employee');

		if(isset($_POST['phone'])) {
			$phone = trim($_POST['phone']);
		}
		else {
			$phone = '';
		}

		if(isset($_POST['email'])) {
			$email = trim($_POST['email']);
		}
		else {
			$email = '';
		}

		if($pref_method == 'phone') {

			if(empty($phone)) {

				// phone is required

				$data = array(
					'status' => 'failure'
				);
			}
			else {

				$insert_status = $db->insertData($name, $phone, $email, $pref_method);

				if($insert_status) {

					// data inserted successfully

					$data = array(
						'status' => 'success'
					);
				}
				else {

					// there was an error processing the request

					$data = array(
						'status' => 'failure'
					);
				}
			}
		}
		else if($pref_method == 'email') {

			if(empty($email)) {

				// email is required

				$data = array(
					'status' => 'failure'
				);
			}
			else {

				$insert_status = $db->insertData($name, $phone, $email, $pref_method);

				if($insert_status) {

					// data inserted successfully

					$data = array(
						'status' => 'success'
					);
				}
				else {

					// there was an error processing the request

					$data = array(
						'status' => 'failure'
					);
				}
			}
		}
		else {
			// unknown pref_method value

			$data = array(
				'status' => 'failure'
			);
		}
	}
	else {

		// pref_method is required

		$data = array(
			'status' => 'failure'
		);
	}
}
else {

	// name is required

	$data = array(
		'status' => 'failure'
	);
}

echo json_encode($data);

?>
