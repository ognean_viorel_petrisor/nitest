<?php

/**
 * Utilities
 */
class Util
{
    /**
     * Format text for HTML output
     *
     * @param string $string
     *
     * @return string
     */
    public static function html($string) {
        return htmlspecialchars($string);
    }
}
