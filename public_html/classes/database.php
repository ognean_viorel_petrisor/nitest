<?php

/**
 * Database access
 */
class Database
{
    //const HOST     = '188.114.115.194';
    //const DATABASE = 'test_company';
    //const USERNAME = 'test_user';
    //const PASSWORD = 'test_company_001';
    const HOST     = 'localhost';
    const DATABASE = 'test_company';
    const USERNAME = 'root';
    const PASSWORD = '';
    
    /** @var Database */
	private static $_instance = null;
    
    /** @var PDO */
    private $_connection = null;
    
    /** @var string[] Names of tables the application is allowed to access */
    private $_tables = array('employee');
    
    /**
     * Constructor
     */
    public function __construct() {
        // Set up database connection
        $dataSourceName = "mysql:host=" . self::HOST . ';dbname=' . self::DATABASE;
        $connection = new PDO($dataSourceName, self::USERNAME, self::PASSWORD);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_connection = $connection;
    }
    
    /**
     * Select unfiltered rows from the database
     *
     * @param $tableName
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public function selectAll($tableName, $limit = 0, $offset = 0) {
        // Checks
        if (!$this->_isValidTable($tableName)) {
            throw new InvalidArgumentException('Invalid table name');
        }
        if (!is_int($limit) || $limit < 0) {
            throw new InvalidArgumentException('Invalid limit value');
        }

        // Create query string
        $sqlQuery = "SELECT * FROM $tableName";

        // Add limit and offset, if needed
        if ($limit > 0) {$sqlQuery .= " LIMIT $limit OFFSET $offset";}

        // Perform query
        $stmt = $this->_executeQuery($sqlQuery);

        // Return data
        return $stmt->fetchAll();
    }
    
    /**
     * Execute SQL query
     *
     * @param string $query
     * @param array|null $data
     *
     * @return PDOStatement
     *
     * @throws PDOException
     */
    private function _executeQuery($query, array $data = null) {
        // Prepare statement
        $stmt = $this->_connection->prepare($query);
        
        // Execute statement
        $stmt->execute($data);

        // Return statement
        return $stmt;
    }
    
    /**
     * Validate table name
     *
     * @param string $table_name
     *
     * @return bool
     */
    private function _isValidTable($table_name) {
        return in_array($table_name, $this->_tables);
    }
    
    /**
     * Singleton accessor
     *
     * @return Database
     */
    public static function get() {
        if (is_null(self::$_instance)) {self::$_instance = new self();}
        return self::$_instance;
    }

    public function insertData($name, $phone, $email, $pref_method) {

        // prepare insert query

        $statement = $this->_connection->prepare("INSERT INTO employee(name, phone, email, preferredContact) VALUES(:name, :phone, :email, :preferredContact)");

        // execute query

        $insert_status = $statement->execute(array(
            "name" => $name,
            "phone" => $phone,
            "email" => $email,
            "preferredContact" => $pref_method,
        ));

        return $insert_status;
    }
}
