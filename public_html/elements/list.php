<?php
    include_once 'classes/database.php';
    include_once 'classes/util.php';
    $db   = Database::get();
    $list = $db->selectAll('employee');
?>
<table class="table_employee">
    <thead>
        <tr>
            <th class="name">Name</th>
            <th class="phone">Phone</th>
            <th class="email">Email</th>
            <th class="preferred_contact">Preferred Contact Method</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($list as $item): ?>
            <tr>
                <td class="name"><?= Util::html($item['name']) ?></td>
                <td class="phone"><?= Util::html($item['phone']) ?></td>
                <td class="email"><?= Util::html($item['email']) ?></td>
                <td class="preferred_contact"><?= Util::html($item['preferredContact']) ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
