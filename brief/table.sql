CREATE TABLE `employee` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NOT NULL,
	`phone` VARCHAR(20) NULL DEFAULT NULL,
	`email` VARCHAR(255) NULL DEFAULT NULL,
	`preferredContact` ENUM('phone','email') NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

